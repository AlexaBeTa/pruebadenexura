import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosListComponent } from './usuarios-list/usuarios-list.component';
import { FormularioComponent } from './formulario/formulario.component';

const routes: Routes = [
  { path: 'listaUsuarios', component: UsuariosListComponent },
  { path: 'editarUsuario/:id', component: FormularioComponent },
  { path: 'crearUsuario', component: FormularioComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
