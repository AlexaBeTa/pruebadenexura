import { Component, OnInit } from '@angular/core';
import { Usuario } from '../clases/usuario';
import { Router, ActivatedRoute } from '@angular/router';
import { EventEmitter, Input, Output } from '@angular/core';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  @Input()  data: any; 
  usuarios: Array<Usuario> = [];
  usuarioNuevo: Usuario = new Usuario();
  usuario: Usuario = new Usuario();
  id:number = 0;
 
  constructor(private router: Router,private route: ActivatedRoute) { 
  this.id = this.route.snapshot.params['id']; 
  console.log(this.id);
  }

  ngOnInit() {
    let usuarioNuevo = new Usuario();
   usuarioNuevo.id = 1;
   usuarioNuevo.nombre = "Gladis fernandez";
   usuarioNuevo.boletin = "si"  ;
   usuarioNuevo.email = "gfernadez@example.com";
   usuarioNuevo.sexo = "femenino";
   this.usuarios.push(usuarioNuevo);
  
   usuarioNuevo = new Usuario();
   usuarioNuevo.id = 2;
   usuarioNuevo.nombre = "felipez gomez";
   usuarioNuevo.boletin = "no"  ;
   usuarioNuevo.email ="fgomez@example.com";
   usuarioNuevo.sexo ="masculino";
   this.usuarios.push(usuarioNuevo);
   
   usuarioNuevo = new Usuario();
   usuarioNuevo.id = 3;
   usuarioNuevo.nombre = "Adriana loaiza";
   usuarioNuevo.boletin = "si" ; 
   usuarioNuevo.email ="aloaiza@example.com";
   usuarioNuevo.sexo ="femenino"
   this.usuarios.push(usuarioNuevo);
   this.usuario = this.usuarios.find(item => item.id == this.id);
   console.log(this.usuario);
  }

}
