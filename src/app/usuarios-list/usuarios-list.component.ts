import { Component, OnInit } from '@angular/core';
import { Usuario } from '../clases/usuario';
import * as $ from 'jquery';
@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.css']
})
export class UsuariosListComponent implements OnInit {
 
  nombre: string;
  boletin: number;
  email: string;
  area: string; 
  sexo: string;
  usuarios: Array<Usuario> = [];
  usuarioNuevo: Usuario = new Usuario();
  usuario: Usuario;

  constructor() { }

  ngOnInit() {  
   let usuarioNuevo = new Usuario();
   usuarioNuevo.id = 1;
   usuarioNuevo.nombre = "Gladis fernandez";
   usuarioNuevo.boletin = "si"  ;
   usuarioNuevo.email = "gfernadez@example.com";
   usuarioNuevo.sexo = "femenino";
   this.usuarios.push(usuarioNuevo);
  
   usuarioNuevo = new Usuario();
   usuarioNuevo.id = 2;
   usuarioNuevo.nombre = "felipez gomez";
   usuarioNuevo.boletin = "no"  ;
   usuarioNuevo.email ="fgomez@example.com";
   usuarioNuevo.sexo ="masculino";
   this.usuarios.push(usuarioNuevo);
   
   usuarioNuevo = new Usuario();
   usuarioNuevo.id = 3;
   usuarioNuevo.nombre = "Adriana loaiza";
   usuarioNuevo.boletin = "si" ; 
   usuarioNuevo.email ="aloaiza@example.com";
   usuarioNuevo.sexo ="femenino"
   this.usuarios.push(usuarioNuevo);
   
   console.log(this.usuarios);
  }

  editarUsuario(id: number) {
   this.usuario = null;
   this.usuario =  new Usuario();
   this.usuario = this.usuarios.find(item => item.id == id);
   console.log(this.usuario);
  }

  eliminarUsuario(id: number) {
    this.usuarios = this.usuarios.filter(item => item.id != id);
    console.log(this.usuarios);
  }

}
